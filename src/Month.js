import React, { useState } from 'react';
import Day from './Day';

const Month = () => {
  const [selectedMonth, setSelectedMonth] = useState(1);

  return (
    <div>
      <select 
        onChange={(e) => setSelectedMonth(e.target.value)}
        value={selectedMonth}
      >
        {[...Array(12).keys()].map((_, i) => (
          <option key={i} value={i + 1}>
            {i + 1}
          </option>
        ))}
      </select>

      <Day year={2021} month={selectedMonth} />
    </div>
  );
};

export default Month;