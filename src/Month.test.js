import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Day from "./Day";

configure({ adapter: new Adapter() });

describe('Calendar tests', () => {
  it("Day component returns correct weekday data for October", () => {
    const wrapper = shallow(<Day year={2021} month={10} />);
    expect(wrapper.find("div").text()).toEqual("Month 10/2021 starts Friday");
  });

  it("Day component returns correct weekday data for July", () => {
    const wrapper = shallow(<Day year={2021} month={7} />);
    expect(wrapper.find("div").text()).toEqual("Month 7/2021 starts Thursday");
  });

  it("Day component returns correct weekday data for December", () => {
    const wrapper = shallow(<Day year={2021} month={12} />);
    expect(wrapper.find("div").text()).toEqual("Month 12/2021 starts Wednesday");
  });
});