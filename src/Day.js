import React from 'react';

const Day = ({ year, month }) => {
  const numericYear = Number(year);
  const numericMonth = Number(month) - 1;  
  const firstDay = new Date(numericYear, numericMonth, 1).getDay();

  const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  const weekdayName = weekdays[firstDay];

  return <div>Month {month}/{year} starts {weekdayName}</div>;
};

export default Day;
